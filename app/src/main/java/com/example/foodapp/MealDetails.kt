package com.example.foodapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.foodapp.api.MealApi
import com.example.foodapp.model.Ingredient
import com.example.foodapp.model.ListDetailsOfMeals
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Response

class MealDetails : AppCompatActivity() {
    private lateinit var mealDetails : ListDetailsOfMeals
    private lateinit var idMeal : String
    private lateinit var meal: Ingredient
    val mealDetailAPI = MealApi.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ingredient_item_details)

        meal = intent.getSerializableExtra("meal") as Ingredient

        var strMeal = findViewById<TextView>(R.id.strMealTextView)
        var strCategory = findViewById<TextView>(R.id.strCategoryTextView)
        var strArea = findViewById<TextView>(R.id.strAreaTextView)
        var strMealThumb = findViewById<ImageView>(R.id.strImageView)
        var strInstructions = findViewById<TextView>(R.id.strInstructionsTextView)

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL

        mealDetailAPI.getResult(meal.idMeal.toString()).enqueue(object: retrofit2.Callback<ListDetailsOfMeals>{

            override fun onFailure(call: Call<ListDetailsOfMeals>, t: Throwable) {
                println("Fail")
            }

            override fun onResponse(call: Call<ListDetailsOfMeals>, response: Response<ListDetailsOfMeals>) {
                println("Response-Meal")
                mealDetails = response.body()!!

                if (mealDetails!=null) {
                    strMeal.text = mealDetails.meals.get(0).strMeal
                    strCategory.text = mealDetails.meals.get(0).strCategory
                    strArea.text = mealDetails.meals.get(0).strArea
                    Picasso.get()
                            .load(mealDetails.meals.get(0).strMealThumb)
                            .into(strMealThumb)
                    strInstructions.text = mealDetails.meals.get(0).strInstructions
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.ingredients_item_details_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.stergeMenuItem){
            println("inside onOptionsItemSelected")
            val intent = Intent()
            intent.putExtra("object", meal)
            setResult(RESULT_OK, intent)
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}