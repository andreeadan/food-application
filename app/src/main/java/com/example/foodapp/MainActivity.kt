package com.example.foodapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.foodapp.adapter.IngredientAdapter
import com.example.foodapp.adapter.OnMealListener
import com.example.foodapp.api.MealApi
import com.example.foodapp.model.Ingredient
import com.example.foodapp.model.ListOfMeals
import retrofit2.Call
import retrofit2.Response

class MainActivity : AppCompatActivity(), View.OnClickListener, OnMealListener{

    private val api = MealApi.create()
    private var ID_ACTIVITY_DETAILS: Int = 1001;
    private lateinit var mealRecyclerView: RecyclerView
    lateinit var listIngredientAdapter: IngredientAdapter
    private lateinit var search: EditText
    private lateinit var searchButton: Button
    private var dataSource= ArrayList<Ingredient>()
    private lateinit var animation: Animation
    private lateinit var foodAnim: ImageView
    private lateinit var incarcareText: TextView
    private lateinit var eroare: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL

        listIngredientAdapter = IngredientAdapter(this, dataSource, this)
        mealRecyclerView = findViewById<RecyclerView>(R.id.ingredienteRecyclerView)
        mealRecyclerView.adapter = listIngredientAdapter
        mealRecyclerView.layoutManager = layoutManager

        search = findViewById(R.id.ingredientEditText)
        searchButton = findViewById(R.id.cautaButton)
        searchButton.setOnClickListener(this)

        incarcareText= findViewById<TextView>(R.id.incarcareTextView)
        eroare = findViewById(R.id.errorTextView)
        eroare.setText("")
    }

    override fun onClick(v: View?) {
        dataSource = ArrayList<Ingredient>()
        foodAnim = findViewById(R.id.foodImageView)
        foodAnim.setVisibility(View.VISIBLE)
        animation = AnimationUtils.loadAnimation(this, R.anim.rotate)
        foodAnim.startAnimation(animation)
        api.getResults(search.text.toString()).enqueue(object: retrofit2.Callback<ListOfMeals>{

            override fun onFailure(call: Call<ListOfMeals>, t: Throwable) {
                println()
                println("Fail")
            }

            override fun onResponse(call: Call<ListOfMeals>, response: Response<ListOfMeals>) {
                println("Response")
                if (response.isSuccessful) {
                    var food: ListOfMeals? = response.body()
                    if (food!!.meals != null) {
                        eroare.setText("");
                        for (meal in food!!.meals) {
                            println(meal.strMeal)
                            dataSource.add(Ingredient(meal.strMeal, meal.strMealThumb, meal.idMeal))
                        }
                        listIngredientAdapter = IngredientAdapter(this@MainActivity, dataSource, this@MainActivity)
                        mealRecyclerView.adapter = listIngredientAdapter
                    }
                    else{
                        eroare.setText("Nu sunt elemente disponibile!");
                    }
                }
            }
        })

        foodAnim.setVisibility(View.GONE)
    }

    ///nu merge onItemClicked fiindca RecyclerView nu are => trebuie implementata o interfata
    override fun onMealClicked(meal: Ingredient, position: Int) {
        incarcareText.setText("încărcare...")
        foodAnim.setVisibility(View.VISIBLE)
        animation.setAnimationListener(object : Animation.AnimationListener{
            override fun onAnimationStart(animation: Animation?) {

            }
            override fun onAnimationEnd(animation: Animation?) {
                foodAnim.setVisibility(View.GONE)
                val intent = Intent(this@MainActivity, MealDetails::class.java)
                val mealDetails = listIngredientAdapter.getIngredient(position) as Ingredient
                intent.putExtra("meal", mealDetails)
                intent.putExtra("position", position)
                startActivityForResult(intent, ID_ACTIVITY_DETAILS)
            }

            override fun onAnimationRepeat(animation: Animation?) {

            }
        })
        foodAnim.startAnimation(animation)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        println("inside on ActivityResult for delete")
        incarcareText.setText("")
        if( requestCode  == ID_ACTIVITY_DETAILS){
            if( resultCode == Activity.RESULT_OK){
                var meal = data?.getSerializableExtra("object") as Ingredient
                dataSource.remove(meal)
                listIngredientAdapter = IngredientAdapter(this@MainActivity, dataSource, this@MainActivity)
                mealRecyclerView.adapter = listIngredientAdapter
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
