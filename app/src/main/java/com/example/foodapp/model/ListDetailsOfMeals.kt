package com.example.foodapp.model

import java.io.Serializable

data class ListDetailsOfMeals (
    var meals:List<DetailsOfMeal>
) : Serializable