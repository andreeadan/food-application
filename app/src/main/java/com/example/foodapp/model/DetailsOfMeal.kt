package com.example.foodapp.model

import java.io.Serializable

data class DetailsOfMeal(
    var strMeal: String,
    var strCategory: String,
    var strArea: String,
    var strMealThumb: String,
    var strInstructions: String
) : Serializable