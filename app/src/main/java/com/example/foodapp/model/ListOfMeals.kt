package com.example.foodapp.model

data class ListOfMeals(
    var meals: ArrayList<Ingredient>
)
