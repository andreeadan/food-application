package com.example.foodapp.model

import java.io.Serializable

data class Ingredient (
        val strMeal: String,
        val strMealThumb: String,
        var idMeal: Float
): Serializable