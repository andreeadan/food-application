package com.example.foodapp.api

import com.example.foodapp.model.ListDetailsOfMeals
import com.example.foodapp.model.ListOfMeals
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface MealApi {
    @GET("filter.php?")
    fun getResults(@Query("i") ingredient:String ): Call<ListOfMeals>

    @GET("lookup.php?")
    //Call<ListDetailsOfMeals>
    fun getResult(@Query("i") idMeal:String ): Call<ListDetailsOfMeals>

    companion object {
        private val httpInterceptor = HttpLoggingInterceptor().apply {
            // there are different logging levels that provide a various amount of detail
            // we will use the most detailed one
            this.level = HttpLoggingInterceptor.Level.BODY
        }
        private val httpClient = OkHttpClient.Builder().apply {
            // add the interceptor to the newly created HTTP client
            this.addInterceptor ( httpInterceptor )
        }.build()

        fun create(): MealApi {
            val retrofit = Retrofit.Builder()
                .baseUrl ( "https://www.themealdb.com/api/json/v1/1/" )
                .addConverterFactory ( GsonConverterFactory.create() )
                .client ( httpClient )
                .build()
            return retrofit.create(MealApi::class.java)
        }
    }
}