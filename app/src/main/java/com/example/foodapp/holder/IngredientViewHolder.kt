package com.example.foodapp.holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.foodapp.R
import com.example.foodapp.adapter.OnMealListener
import com.example.foodapp.model.Ingredient
import com.squareup.picasso.Picasso

class IngredientViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{
    private var ingredientNameTextView: TextView
    private var ingredientPicImageView: ImageView
    private lateinit var dataCache: Ingredient
    lateinit var onmeal: OnMealListener

    init {
        ingredientNameTextView = itemView.findViewById(R.id.numeIngredientTextView)
        ingredientPicImageView = itemView.findViewById(R.id.imagineIngredientImageView)
        itemView.setOnClickListener(this)
    }

    fun bindData(data: Ingredient){
        dataCache = data
        ingredientNameTextView.text = data.strMeal

        Picasso.get()
                .load(data.strMealThumb)
                .into(ingredientPicImageView)
    }

    fun bind(ingredient: Ingredient,clickListener: OnMealListener)
    {
        itemView.setOnClickListener {
                clickListener.onMealClicked(ingredient, adapterPosition)
        }
    }

    override fun onClick(v: View?) {
    }
}

