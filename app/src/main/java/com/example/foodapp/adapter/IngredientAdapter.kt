package com.example.foodapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.Toast
import androidx.loader.content.AsyncTaskLoader
import androidx.recyclerview.widget.RecyclerView
import com.example.foodapp.R
import com.example.foodapp.holder.IngredientViewHolder
import com.example.foodapp.model.Ingredient

class IngredientAdapter(
        private val context: Context,
        private val dataSource: ArrayList<Ingredient>,
        private val mealListener: OnMealListener
) : RecyclerView.Adapter<IngredientViewHolder>() {

    // get a reference to the LayoutInflater service
    private val inflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientViewHolder {
        var rowView = inflater.inflate(R.layout.ingredient_item, parent, false)
        return IngredientViewHolder(rowView)
    }

    override fun onBindViewHolder(holder: IngredientViewHolder, position: Int) {
        holder.bindData(dataSource.elementAt(position))
        holder.bind(dataSource.elementAt(position), mealListener)
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    public fun getIngredient(position: Int): Any {
        return dataSource.elementAt(position)
    }
}

public interface OnMealListener{
    fun onMealClicked(meal: Ingredient, position: Int)
}